# TimeStamp service

Manual testing:

```bash
curl -X POST -H "Content-Type: text/plain" -d "$(date +%s)" http://localhost:8080/in
```

```bash
curl localhost:8080/out
```

Wrong POST

```bash
curl -X POST -H "Content-Type: application/json" -d "{"asd": "$(date +%s)"}" http://localhost:8080/in
```
