package main

import (
	"bytes"
	"fmt"
	"io"
	"log"
	"net/http"
	"strconv"
	"time"
)

// TimeStampWrapper is a basic wrapper struct to pass LatestTimeStamp as a reference
type TimeStampWrapper struct {
	LatestTimeStamp *time.Time
}

func main() {

	mq := make(chan *time.Time)
	wrap := &TimeStampWrapper{
		LatestTimeStamp: nil,
	}
	go updateBuffer(mq, wrap)
	go client()

	http.HandleFunc("/in", inHandler(mq))
	http.HandleFunc("/out", outHandler(wrap))
	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		log.Fatalln(err)
	}
}

// inHandler is a function which receive a channel and return a HandleFunc, the actual handler
func inHandler(ch chan<- *time.Time) func(w http.ResponseWriter, req *http.Request) {
	return func(w http.ResponseWriter, req *http.Request) {
		// check if HTTP method is POST, error if it is not
		if req.Method != http.MethodPost {
			http.Error(w, "only POST method is allowed", http.StatusMethodNotAllowed)
			return
		}
		// parse body, convert unix timestamp to bytes, string and int64
		body, err := io.ReadAll(req.Body)
		if err != nil {
			log.Fatalln(err)
		}
		receivedTimeString := string(body)
		receivedTimeInt64, err := strconv.ParseInt(receivedTimeString, 10, 64)
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		// convert unix timestamp int64 to time.Time
		receivedTime := time.Unix(receivedTimeInt64, 0)
		// pass received time stamp to the channel
		ch <- &receivedTime
		// respond everything is OK
		w.WriteHeader(http.StatusOK)
		_, err = w.Write([]byte("unix timestamp saved: " + receivedTimeString))
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}
}

// outHandler is a function which receive a TimeStampWrapper, and return a HandleFunc, the actual handler
func outHandler(wrap *TimeStampWrapper) func(w http.ResponseWriter, req *http.Request) {
	return func(w http.ResponseWriter, req *http.Request) {
		// check if HTTP method is GET, error if is not
		if req.Method != http.MethodGet {
			http.Error(w, "only GET method is allowed", http.StatusMethodNotAllowed)
			return
		}
		// check if wrap or it's content is not nil
		if wrap == nil || wrap.LatestTimeStamp == nil {
			http.Error(w, "no timestamp provided yet", http.StatusNotFound)
			return
		}
		// create string from given timestamp
		now := strconv.FormatInt(wrap.LatestTimeStamp.Unix(), 10)

		// respond the given timestamp and 200 OK
		w.WriteHeader(http.StatusOK)

		_, err := w.Write([]byte(now))
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}
}

// updateBuffer is a helper function, receive a one directional channel and a wrapped timestamp
// loop while channel is open
// pass the given timestamp from channel to the wrapped timestamp, so it can store the latest timestamp
func updateBuffer(ch <-chan *time.Time, wrap *TimeStampWrapper) {
	opened := true
	for opened {
		wrap.LatestTimeStamp, opened = <-ch
	}
}

// client is for test functionality
// it waits 2 second, and then provide a timestamp to the application
// after that, it calls /out endpoint and print out the given timestamp
func client() {
	time.Sleep(time.Second * 2)
	now := strconv.FormatInt(time.Now().Unix(), 10)

	resp, err := http.Post("http://localhost:8080/in", "text/plain", bytes.NewBufferString(now))
	if err != nil {
		log.Println(err)
		return
	}

	if resp.StatusCode != http.StatusOK {
		log.Println("can't post timestamp, response code: " + resp.Status)
	}

	resp, err = http.Get("http://localhost:8080/out")
	if err != nil {
		log.Println(err)
		return
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Println(string(body))
}
