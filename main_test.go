package main

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"
	"time"
)

func postRequestHelper(str string, t *testing.T) *http.Request {
	req, err := http.NewRequest("POST", "http://localhost:8080/in", bytes.NewBufferString(str))
	if err != nil {
		t.Fatal(err)
	}
	return req
}

func getRequestHelper(t *testing.T) *http.Request {
	req, err := http.NewRequest("GET", "http://localhost:8080/out", nil)
	if err != nil {
		t.Fatal(err)
	}
	return req
}

// inHandlerHelper is a helper function, it creates a request, a handler, and return them and an error
// if something goes wrong
func inHandlerHelper(ch chan<- *time.Time, now *time.Time) (http.HandlerFunc, *http.Request, error) {
	nowStr := strconv.FormatInt(now.Unix(), 10)
	req, err := http.NewRequest("POST", "http://localhost:8080/in", bytes.NewBufferString(nowStr))
	if err != nil {
		return nil, nil, err
	}
	handler := http.HandlerFunc(inHandler(ch))
	return handler, req, nil
}

// inHandlerTests wrap some test case to prevent code duplication
// test if status code matches with expected status code
// test if body matches with expected body
func inHandlerTests(now *time.Time, responseRecorder *httptest.ResponseRecorder, t *testing.T) {
	if status := responseRecorder.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
	nowStr := strconv.FormatInt(now.Unix(), 10)
	expected := "unix timestamp saved: " + nowStr
	if responseRecorder.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			responseRecorder.Body.String(), expected)
	}
}

// outHandlerHelper is a helper function, it creates a request, a handler, and return them and an error
// if something goes wrong
func outHandlerHelper(wrap *TimeStampWrapper) (http.HandlerFunc, *http.Request, error) {
	req, err := http.NewRequest("GET", "http://localhost:8080/out", nil)
	if err != nil {
		return nil, nil, err
	}
	handler := http.HandlerFunc(outHandler(wrap))
	return handler, req, nil
}

// outHandlerTests wrap some test case to prevent code duplication
// test if status code matches with expected status code
// test if body matches with expected body
func outHandlerTests(now *time.Time, responseRecorder *httptest.ResponseRecorder, t *testing.T) {
	if status := responseRecorder.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
	expected := strconv.FormatInt(now.Unix(), 10)
	if responseRecorder.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			responseRecorder.Body.String(), expected)
	}
}

// setup is a helper function, prevent code duplication
// creates a test channel and
// creates a test wrap for timestamp
func setup() (chan *time.Time, *TimeStampWrapper) {
	testChannel := make(chan *time.Time)
	testWrap := &TimeStampWrapper{LatestTimeStamp: nil}

	return testChannel, testWrap
}

// TestInHandler tests inHandler functionality
// creates a request, send it, and check if the response is the expected
func TestInHandler(t *testing.T) {
	testChannel, testWrap := setup()
	go func() {
		opened := true
		for opened {
			testWrap.LatestTimeStamp, opened = <-testChannel
		}
	}()
	now := time.Now()

	handler, req, err := inHandlerHelper(testChannel, &now)
	if err != nil {
		t.Fatal(err)
	}

	responseRecorder := httptest.NewRecorder()
	handler.ServeHTTP(responseRecorder, req)
	inHandlerTests(&now, responseRecorder, t)
}

// TestOutHandler tests outHandler functionality
// creates a request, send it, and check if the response is the expected
func TestOutHandler(t *testing.T) {
	now := time.Now()
	testWrap := &TimeStampWrapper{LatestTimeStamp: &now}
	handler, req, err := outHandlerHelper(testWrap)
	if err != nil {
		t.Fatal(err)
	}
	responseRecorder := httptest.NewRecorder()
	handler.ServeHTTP(responseRecorder, req)
	outHandlerTests(&now, responseRecorder, t)
}

// TestOutHandlerPost tests if inHandler got a wrong HTTP method
func TestOutHandlerPost(t *testing.T) {
	req, err := http.NewRequest("POST", "http://localhost:8080/out", nil)
	if err != nil {
		t.Fatal(err)
	}
	responseRecorder := httptest.NewRecorder()
	handler := http.HandlerFunc(outHandler(nil))
	handler.ServeHTTP(responseRecorder, req)
	if status := responseRecorder.Code; status != http.StatusMethodNotAllowed {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusMethodNotAllowed)
	}
}

// TestUnSetTimeStamp tests outHandler if timestamp previously not set
func TestUnSetTimeStamp(t *testing.T) {
	req, err := http.NewRequest("GET", "http://localhost:8080/out", nil)
	if err != nil {
		t.Fatal(err)
	}
	responseRecorder := httptest.NewRecorder()
	handler := http.HandlerFunc(outHandler(nil))
	handler.ServeHTTP(responseRecorder, req)
	if status := responseRecorder.Code; status != http.StatusNotFound {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusNotFound)
	}
}

// TestSuccessfulSaveAndRead is basically the client function
// it runs a full test, create a timestamp, post it, after try to retrieve it from the server
func TestSuccessfulSaveAndRead(t *testing.T) {
	testChannel, testWrap := setup()

	go updateBuffer(testChannel, testWrap)

	now := time.Now()
	inHandler, reqIn, err := inHandlerHelper(testChannel, &now)
	if err != nil {
		t.Fatal(err)
	}
	inResponseRecorder := httptest.NewRecorder()
	inHandler.ServeHTTP(inResponseRecorder, reqIn)
	inHandlerTests(&now, inResponseRecorder, t)

	outResponseRecorder := httptest.NewRecorder()
	outHandler, reqOut, err := outHandlerHelper(testWrap)
	if err != nil {
		t.Fatal(err)
	}
	outHandler.ServeHTTP(outResponseRecorder, reqOut)
	outHandlerTests(&now, outResponseRecorder, t)
}

// TestParallelActions is a semi-finished test
// it should test if the server can handle parallel POST and GET requests
//func TestParallelActions(t *testing.T) {
//	testChannel, testWrap := setup()
//	go updateBuffer(testChannel, testWrap)
//
//	inHandler := http.HandlerFunc(inHandler(testChannel))
//	inResponseRecorder := httptest.NewRecorder()
//	inHandler.ServeHTTP(inResponseRecorder, reqIn)
//}
